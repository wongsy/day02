/**
 * Created by s25854 on 15/10/2016.
 */
// Create a Employee class with attributes and methods

var Employee = {
    //attributes
    firstName: "Bob",
    lastName: "Tan",
    employeeNumber: 12345,
    salary: 3000.80,
    age: 30,
    department: "IT",

    //methods
    getFullName: function() {return this.firstName + " " + this.lastName},

    isRich: function() {
        if (this.salary > 2000){
            return true;
        } else {
            return false;
        }
    },

    belongsToDepartment: function(s) {
        return (s == this.department);
    }
};

console.log("Name: " + Employee.getFullName());
console.log("Belongs to IT dept? " + Employee.belongsToDepartment("IT"));
console.log("Is rich? " + Employee.isRich());