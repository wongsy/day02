/**
 * Created by s25854 on 11/10/2016.
 */
var START = 99;
var END = 0;
console.log("Lyrics of the song " + START + " Bottles of Beer\n");

for (var i = START; i > END; i--){
    console.log(i + " bottles of beer on the wall, " + i + " bottles of beer.");
    if (i > 1)
         console.log("Take one down and pass it around, " + (i - 1 ) + " bottles of beer on the wall.\n");
    else
        console.log("Take one down and pass it around, no more bottles of beer on the wall.\n");
}
console.log("No more bottles of beer on the wall, no more bottles of beer.");
console.log("Go to the store and buy some more, 99 bottles of beer on the wall.");