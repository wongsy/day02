/**
 * Created by s25854 on 11/10/2016.
 */
/* Objective:
 Write a program that expects a number N, and creates an array A with elements 0 - N
 now write a function that calculates the sum of the array */

const N = 5;
var sum = 0;
var array = [];

for (var i = 0; i < N; i++){
    //array.push(parseInt(prompt("Enter value " + (i + 1))));
    //Note: prompt will not work in server side
    array.push(i);
}
sumArray();
console.log("The sum of elements in the array is " + sum);

function sumArray(){
    array.forEach(function (element) {
        sum += element;
    })
}

