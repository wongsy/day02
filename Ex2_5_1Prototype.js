/**
 * Created by s25854 on 15/10/2016.
 */
// Coding in progress ....

// Create a Person class which will be used to extend Employee class created previously
// Try to move properties in Employee Class to person class

var Employee = function(){
    //attributes
    firstName = "Bob",
    lastName = "Tan",
    employeeNumber = 12345,
    salary = 3000.80,
    age = 30,
    department = "IT",

    //methods
    getFullName = function() {return this.firstName + " " + this.lastName},

    isRich = function() {
        if (this.salary > 2000){
            return true;
        } else {
            return false;
        }
    },

    belongsToDepartment = function(s) {
        return (s == this.department);
    }
};
// var Person = function(){
// }
// Person.prototype = Object.create(Employee.prototype);
//
// Person.prototype.getFullName = function() {
//     return "Name: " +this.firstName + " " + this.lastName;
// };
// Person.prototype.isRich = function() {
//     if (this.salary > 2000){
//         return this.name + " is rich";
//     } else {
//         return this.name + " is not rich yet";;
//     }
// };
//
// var pers = new Person();


// console.log("Employee Name: " + Employee.getFullName);
// // console.log("Employee belongs to IT dept? " + Employee.belongsToDepartment("IT"));
// console.log("Employee is rich? " + Employee.isRich);

// console.log(pers.getFullName());
// console.log(pers.belongsToDepartment("IT"));
// console.log(pers.isRich());