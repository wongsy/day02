/**
 * Created by s25854 on 11/10/2016.
 */
/* Objective:
 Write a program that expects a number N, and creates an array A with elements 0 - N,
 Now write a function that expects a number and an array and returns the index of that element
 */
const N = 5;
var sum = 0;
var array = [];
var num = 2;

for (var i = 0; i < N; i++){
    //array.push(parseInt(prompt("Enter value " + (i + 1))));
    //Note: prompt will not work in server side
    array.push(i);
}
var pos = findElement(num, array);

if ( pos > -1) {
    console.log("Index of %s is %s", num, pos);
} else {
    console.log(num + " not found");
}

function findElement(num, arr){
    var found = -1;
    array.forEach(function (element, index) {
        if (element == num){
            found = index;
        }
    });
    return found;
}