/**
 * Created by s25854 on 11/10/2016.
 */
/* Write a function that expects three parameters: number, successCallback, errorCallback
If the number entered is greater than 18, execute successCallback else execute errorCallback
*/
//--------- Method 1 ----------
var successCallback = function(){
    console.log("Success");
}
var errorCallback =  function(){
    console.log("error");
}

function func1(number, sCallback, errCallback){
    if (number > 18) {
        sCallback();
    } else {
        errCallback();
    }
}
func(26, successCallback, errorCallback); //prints success
func(16, successCallback, errorCallback); //prints error

//---------- Method 2 ----------------
function func2(number, sCallback, errCallback){
    if (number > 18) {
        sCallback();
    } else {
        errCallback();
    }
}
//anonymous functions
// prints error
func(16,
    function(){
        console.log("Success");
    },
    function(){
        console.log("error");
    }
);
// prints Legal age
func(26,
    function(){
        console.log("Legal age");
    },
    function(){
        console.log("Too Young");
    }
);